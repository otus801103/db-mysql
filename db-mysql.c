//gcc -o db-mysql db-mysql.c $(mysql_config --cflags --libs) -Wall -Wextra -Wpedantic -std=c11
#include <mysql.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    if (argc < 6) {
        fprintf(stderr, "Usage: %s <host> <user> <password> <database_name> <table_name> <column_name>\n", argv[0]);
        return 1;
    }

    const char *host = argv[1];
    const char *user = argv[2];
    const char *password = argv[3];
    const char *databaseName = argv[4];
    const char *tableName = argv[5];
    const char *columnName = argv[6];
    unsigned int port = 3306;
    const char *unixSocket = NULL;
    unsigned long clientFlag = 0;
    // Инициализируем объект соединения с MySQL
    MYSQL *conn = mysql_init(NULL);
    if (conn == NULL) {
        fprintf(stderr, "mysql_init() failed\n");
        return 1;
    }
    // Соединяемся с базой данных MySQL, используя учетные данные и название базы данных
    if (mysql_real_connect(conn, host, user, password, databaseName, port, unixSocket, clientFlag) == NULL) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        return 1;
    }
    // Формируем SQL-запрос
    char query[512];
    snprintf(query, sizeof(query),
             "SELECT AVG(%s), MAX(%s), MIN(%s), SUM(%s), "
             "VARIANCE(%s) FROM %s;",
             columnName, columnName, columnName, columnName, columnName, tableName);
    // Отправляем SQL-запрос в базу данных
    if (mysql_query(conn, query)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        return 1;
    }
    // Получаем и сохраняем результат
    MYSQL_RES *result = mysql_store_result(conn);
    if (result == NULL) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        return 1;
    }
    // Получаем ряд из набора результатов, выводим полученные значения, если по значению не получено строк , выводим "NULL"
    MYSQL_ROW row = mysql_fetch_row(result);
    if (row) {
        printf("Average: %s\n", row[0] ? row[0] : "NULL");
        printf("Maximum: %s\n", row[1] ? row[1] : "NULL");
        printf("Minimum: %s\n", row[2] ? row[2] : "NULL");
        printf("Sum: %s\n", row[3] ? row[3] : "NULL");
        printf("Variance: %s\n", row[4] ? row[4] : "NULL");
    } else {
        fprintf(stderr, "No rows returned.\n");
    }

    mysql_free_result(result);
    mysql_close(conn);
    return 0;
}
